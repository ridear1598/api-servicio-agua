import axios from "axios";

const getEmpresasSoporte = async (doc: string) => {
    try {
      let url = "http://localhost:4000/api/v1/soporte_licencias";
      return await axios.post(url, { documento: doc });
    } catch (error) {
      console.error(error);
    }
  };

  export default getEmpresasSoporte
  