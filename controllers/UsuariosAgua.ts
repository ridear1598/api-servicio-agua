import { Request, Response } from "express";
import pool from "../db/connections";

export const getUsuariosAgua = async (req: Request, res: Response) => {
    try {
      const pools = await pool 
      let resp = await pools.query(`select * from usuariosagua`);
     
      res.json({
        msg: "Exito",
        status: 1,
        lista: resp.rows,
      });
    } catch (err) {
          console.log(err);
      res.status(404).json({
          msg: "Error",
      });
    }
  };
  export const addNewUser=async (req: Request, res: Response) => {
    try {
       const pools = await pool ;
       const consulta="insert INTO  usuariosagua(n_i_f, categoria,usuario_propietario,dni,direccion,estado,ultimo_pago) values ($1,$2,$3,$4, $5,$6,$7) RETURNING *"
       const values = [req.body.n_i_f, req.body.categoria,req.body.usuario_propietario.toUpperCase(), req.body.dni,req.body.direccion.toUpperCase(),req.body.estado,req.body.ultimo_pago];
      
       console.log('rsp',req.body.usuario_propietario, req.body.direccion);
       
       if(req.body.usuario_propietario==null || req.body.direccion==null ){
        return res.json({
          msg: "Ingresar nombre y direccion del Usuario",
          status: 0,
          lista: [],
        });
      }  
       const resp = await pools.query(consulta, values);
      if(resp.rowCount==1){
         return res.json({
            msg: "Usario Insertado correctamente",
            status: 1,
            lista: resp.rows,
          });
        }

        return res.json({
          msg: "Errror al insertar usuario",
          status: 1,
          lista: resp.rows,
        })
  
    } catch (error) {
      console.log('Error al insertar',error);
      
    }
  }
  export const updateUser =  async(req: Request, res: Response)=>{
    try {
      const pools = await pool ;
      const consulta=`update usuariosagua set dni='${req.body.dni}',direccion='${req.body.direccion.toUpperCase()}' ,usuario_propietario='${req.body.usuario_propietario.toUpperCase()}' ,alquilan_servicio='${req.body.alquilan_servicio}' where id=${req.body.id}`

      const resp = await pools.query(consulta);
      if(resp.rowCount==1){
        return res.json({
          msg: "Usario Actualizado correctamente",
          status: 1,
          lista: resp.rows,
        });
      }
      
      return res.json({
        msg: "Errror al actualizar usuario",
        status: 1,
        lista: resp.rows,
      })    
    } catch (error) {
      console.log('Error al insertar',error)
    }
  
  }