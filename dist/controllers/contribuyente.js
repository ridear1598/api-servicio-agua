"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getContribuyentes = void 0;
const api_1 = __importDefault(require("../db/api"));
const connections_1 = __importDefault(require("../db/connections"));
const getContribuyentes = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let body = req.body;
        let params = req.params;
        const lista = yield api_1.default(body.doc_empresa);
        let result = lista === null || lista === void 0 ? void 0 : lista.data;
        let empresa = result.lista.find((data) => (data.IDCLIEPROV = body.doc_empresa));
        if (empresa === undefined) {
            return res.json({
                msg: "La empresa no cuenta con una licencia de soporte",
                status: 0,
            });
        }
        if ((empresa != undefined && empresa.id_secret != params.id) ||
            empresa.key_soporte != params.clave) {
            res.status(400);
            return res.json({
                msg: "Clave o id incorrectos",
                status: 0,
            });
        }
        if (empresa != undefined && empresa.estado === false) {
            res.status(400);
            return res.json({
                msg: "La licencia de soporte de la empresa se ha vencido, renovar licencia",
                status: 0,
            });
        }
        if (empresa != undefined && empresa.estado === true) {
            const pools = yield connections_1.default;
            let resp = yield pools.query(`select * from contribuyente where ruc = '${body.documento}'`);
            guardarHistorial_consultas(body.usario, body.documento, body.doc_empresa);
            res.json({
                msg: "Exito",
                status: 1,
                lista: resp.rows,
            });
        }
    }
    catch (error) {
        //console.log(error);
        res.status(404).json({
            msg: "Error",
        });
    }
});
exports.getContribuyentes = getContribuyentes;
const guardarHistorial_consultas = (user, documento, doc_empresa) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        var fecha = new Date().toISOString();
        let fech = new Date(fecha);
        const text = "insert into historial_consultas (fecha,usuario,doc_buscado,empresa) values ($1,$2,$3,$4) RETURNING *";
        const values = [fecha, user, documento, doc_empresa];
        const pools = yield connections_1.default;
        const res = yield pools.query(text, values);
        //console.log(res.rows[0]);
    }
    catch (err) {
        console.log(err.stack);
    }
});
//# sourceMappingURL=contribuyente.js.map