"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const contribuyente_1 = require("../controllers/contribuyente");
const router = express_1.Router();
router.get("/api/contribuyentes/:id/:clave", contribuyente_1.getContribuyentes);
exports.default = router;
//# sourceMappingURL=contribuyente.js.map