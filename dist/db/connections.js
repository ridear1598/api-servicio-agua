"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
const config = {
    user: "postgres",
    host: "localhost",
    database: "ruc_ps",
    password: "123",
    port: 5432,
};
const pool = new pg_1.Pool(config)
    .connect()
    .then((client) => {
    console.log("connected BD");
    return client;
})
    .catch((err) => console.error("error connecting", err.stack));
exports.default = pool;
//# sourceMappingURL=connections.js.map