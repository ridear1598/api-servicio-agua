"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const contribuyente_1 = __importDefault(require("../routes/contribuyente"));
const historial_1 = __importDefault(require("../routes/historial"));
class server {
    constructor() {
        this.apiRoutes = {
            historial: "/api/historial",
        };
        this.app = express_1.default();
        this.port = process.env.PORT || "3030";
        this.middlewares();
        this.routes();
    }
    middlewares() {
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json());
    }
    routes() {
        this.app.use(contribuyente_1.default);
        this.app.use(this.apiRoutes.historial, historial_1.default);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log("Servidor corriendo en:" + this.port);
        });
    }
}
exports.default = server;
//# sourceMappingURL=server.js.map