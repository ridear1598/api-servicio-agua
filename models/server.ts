import express, { Application } from "express";
import cors from "cors";
import routerUsuariosAgua from "../routes/usuariosAgua"

class server {
  private app: Application;
  private port: string;
  private apiRoutes = {
    historial: "/api/historial",
  };
  constructor() {
    this.app = express();
    this.port = process.env.PORT || "3030";
    this.middlewares();
    this.routes();
  }
  middlewares() {
    this.app.use(cors());
    this.app.use(express.json());
  }
  routes() {
    this.app.use(routerUsuariosAgua);
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("Servidor corriendo en:" + this.port);
    });
  }
}

export default server;
