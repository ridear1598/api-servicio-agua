import { Router } from "express";
import { getUsuariosAgua, updateUser } from "../controllers/UsuariosAgua";
import { addNewUser } from "../controllers/UsuariosAgua";

const router = Router();

router.get("/api/usuarios_agua", getUsuariosAgua);
router.post("/api/usuarios_agua/add", addNewUser);
router.put("/api/usuarios_agua/update",updateUser)

export default router;
